package org.zahnleiter.casl4j.spring;

import com.tngtech.archunit.core.domain.JavaClasses;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.springframework.web.bind.annotation.RestController;
import org.zahnleiter.casl4j.archcheck.AbstractArchCheckGradlePlugin;
import org.zahnleiter.casl4j.domain.usecase.UseCase;

/**
 * A Gradle plug-in that checks the application code for compliance with Clean
 * Architecture rules.
 * There is a separate architecture check plug-in for the domain logic.
 * However, each technology specific implementation of CASL4J (in this example
 * implementation for Spring) needs its own application check plug-in as this is
 * technology specific. For example Spring uses its own annotation the plug-in
 * can look for. These annotations may differ from annotations used by Micronaut
 * or Quarkus.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class ApplicationArchitectureCheckGradlePlugin extends AbstractArchCheckGradlePlugin {

    @Override
    protected String taskName() {
        return "springApplicationArchitectureCheck";
    }

    @Override
    protected void architectureChecks(final JavaClasses classes) {
        classes().that().areAnnotatedWith(UseCase.class) //
                .should().onlyBeAccessed().byClassesThat().areAnnotatedWith(RestController.class);
    }

}
