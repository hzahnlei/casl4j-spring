package org.zahnleiter.casl4j.spring;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * We need this class so that our {@link CaslAnnotatedBeanFactory} can
 * instantiate domain logic classes as Spring beans.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@SpringBootApplication
// Needs to be a class even though abstract, cannot be an interface.
@SuppressWarnings("java:S1610")
public abstract class CaslSpringBootApplication {
}
