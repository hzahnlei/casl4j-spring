package org.zahnleiter.casl4j.spring.usecase;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.zahnleiter.casl4j.domain.usecase.AbstractBusinessTransaction;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransaction;

/**
 * Implementation of {@link BusinessTransaction} for the Spring
 * framework.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public class BussinessTransactionImpl extends AbstractBusinessTransaction {

    private final PlatformTransactionManager txManager;

    private final TransactionStatus txStatus;

    public BussinessTransactionImpl(final PlatformTransactionManager txManager, final TransactionStatus txStatus) {
        this.txManager = txManager;
        this.txStatus = txStatus;
    }

    @Override
    protected void doCommit() {
        this.txManager.commit(this.txStatus);
    }

    @Override
    protected void doRollback() {
        this.txManager.rollback(this.txStatus);
    }

    @Override
    public boolean needsCompletion() {
        return !this.txStatus.isCompleted();
    }

}
