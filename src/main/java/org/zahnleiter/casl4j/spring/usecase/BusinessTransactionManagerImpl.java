package org.zahnleiter.casl4j.spring.usecase;

import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.zahnleiter.casl4j.domain.usecase.AbstractBusinessTransaction;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransactionManager;

/**
 * Implementation of {@link BusinessTransactionManager} for the Spring
 * framework.
 * <p>
 * You can use the transaction manager directly/programmaticaly in case you do
 * not want to or cannot rely on the
 * {@link org.zahnleiter.casl4j.domain.usecase.BusinessTransaction} annotation.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Component
public class BusinessTransactionManagerImpl implements BusinessTransactionManager {

    private static final TransactionDefinition DEFAULT_TX_PARAMS = new DefaultTransactionDefinition();

    private final PlatformTransactionManager txManager;

    public BusinessTransactionManagerImpl(final PlatformTransactionManager txManager) {
        this.txManager = txManager;
    }

    @Override
    public AbstractBusinessTransaction beginNewBusinessTransaction() {
        return new BussinessTransactionImpl(this.txManager, this.txManager.getTransaction(DEFAULT_TX_PARAMS));
    }

}
