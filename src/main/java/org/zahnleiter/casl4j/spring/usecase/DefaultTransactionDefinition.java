package org.zahnleiter.casl4j.spring.usecase;

import org.springframework.transaction.TransactionDefinition;

/**
 * Default "configuration" or properties of transactions.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
final class DefaultTransactionDefinition implements TransactionDefinition {
    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public int getTimeout() {
        return TransactionDefinition.TIMEOUT_DEFAULT;
    }

    @Override
    public int getPropagationBehavior() {
        return TransactionDefinition.PROPAGATION_REQUIRED;
    }

    @Override
    public String getName() {
        return "business transaction";
    }

    @Override
    public int getIsolationLevel() {
        return TransactionDefinition.ISOLATION_READ_UNCOMMITTED;
    }
}