package org.zahnleiter.casl4j.spring.usecase;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransactionManager;

/**
 * Ensures that methods annotated with
 * {@link org.zahnleiter.casl4j.domain.usecase.BusinessTransaction} are
 * automatically committed on success. The are automatically rolled back in case
 * of failure.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Aspect
@Component
public class BusinessTransactionAspect {

    private final BusinessTransactionManager txManager;

    public BusinessTransactionAspect(final BusinessTransactionManager txManager) {
        this.txManager = txManager;
    }

    @Around("@annotation(org.zahnleiter.casl4j.domain.usecase.BusinessTransaction)")
    public Object businessTransaction(final ProceedingJoinPoint joinPoint) throws Throwable {
        try (final var tx = this.txManager.beginNewBusinessTransaction()) {
            final Object result = joinPoint.proceed();
            tx.commit();
            return result;
        }
        // In case of failure auto-close will roll back automatically.
    }

}
