package org.zahnleiter.casl4j.spring;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.zahnleiter.casl4j.common.CommonComponent;
import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.domain.usecase.UseCase;

/**
 * CASL4J's annotations are technology agnostic. Hence, classes that are
 * annotated with CASL4J annotations are not automatically recognized by Spring
 * as beans that can be created or injected.
 * <br>
 * This piece of code makes CASL4J annotated classes become Spring beans. As a
 * benefit, we do not have to manually instatiate domain logic classes manually
 * (Spring configuration). Domain logic classes behave as if they were annotated
 * with Spring annotations such as {@link Component} or {@link Service}.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Configuration
public class CaslAnnotatedBeanFactory {

    /**
     * @param app          {@link CaslSpringBootApplication} is an abstract base
     *                     class. The object passed in here is the actual Spring
     *                     Boot application deriving from
     *                     {@link CaslSpringBootApplication}. We use its package for
     *                     component scanning for CASL annotated components.
     * @param beanRegistry Given by Spring Framework.
     * @return A custom bean factory that creates beans from classes that were not
     *         annotated with Spring annotations (for example {@link Service} or
     *         {@link Component}) but with annotations from the CASL framework such
     *         as {@link DomainService}, {@link UseCase} or {@link CommonComponent}.
     */
    @Bean
    BeanFactoryPostProcessor beanFactoryPostProcessor(final CaslSpringBootApplication app,
            final ApplicationContext beanRegistry) {
        return beanFactory -> {
            createBeansFromAnnotatedClasses((BeanDefinitionRegistry) beanFactory, app.getClass().getPackageName());
            createCaslBeansFromAnnotatedClasses((BeanDefinitionRegistry) beanFactory,
                    CommonComponent.class.getPackageName());
        };
    }

    private void createBeansFromAnnotatedClasses(final BeanDefinitionRegistry beanRegistry,
            final String appsBasePackage) {
        final ClassPathBeanDefinitionScanner beanDefinitionScanner = new ClassPathBeanDefinitionScanner(beanRegistry);
        beanDefinitionScanner.addIncludeFilter(allowUseCaseAndDomainService());
        beanDefinitionScanner.scan(appsBasePackage);
    }

    private static TypeFilter allowUseCaseAndDomainService() {
        return (final MetadataReader mr, final MetadataReaderFactory mrf) -> {
            return mr.getAnnotationMetadata().isAnnotated(UseCase.class.getName())
                    || mr.getAnnotationMetadata().isAnnotated(DomainService.class.getName())
                    || mr.getAnnotationMetadata().isAnnotated(CommonComponent.class.getName());
        };
    }

    private void createCaslBeansFromAnnotatedClasses(final BeanDefinitionRegistry beanRegistry,
            final String caslBasePackage) {
        final ClassPathBeanDefinitionScanner beanDefinitionScanner = new ClassPathBeanDefinitionScanner(beanRegistry);
        beanDefinitionScanner.addIncludeFilter(allowCommonComponents());
        beanDefinitionScanner.scan(caslBasePackage);
    }

    private static TypeFilter allowCommonComponents() {
        return (final MetadataReader mr, final MetadataReaderFactory mrf) -> {
            return mr.getAnnotationMetadata().isAnnotated(CommonComponent.class.getName());
        };
    }

}
