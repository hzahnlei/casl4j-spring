package org.zahnleiter.casl4j.spring.entity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.zahnleiter.casl4j.domain.entity.BeanValidated;

/**
 * Registers the {@link BeanValidated} annotation with spring so that JSR 380
 * bean validation is applied by Spring when instructed to do so.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Configuration
public class BeanValidationConfiguration {

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        final var postProcessor = new MethodValidationPostProcessor();
        postProcessor.setValidatedAnnotationType(BeanValidated.class);
        return postProcessor;
    }

}
