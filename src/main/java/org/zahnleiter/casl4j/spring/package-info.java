/**
 * <pre>
 *  #####    ###    #####  ##           ##      ##  S
 * ##   ##  ## ##  ##   ## ##          ##       ##  P
 * ##      ##   ## ##      ##          ##       ##  R
 * ##      #######  #####  ##         ##        ##  I
 * ##      ##   ##      ## ##        ##         ##  N
 * ##   ## ##   ## ##   ## ##        ##    ##   ##  G
 *  #####  ##   ##  #####  #######  ##      #####
 *
 * Clean Architecture Support Library for Java - Spring
 * (c) 2022 Holger Zahnleiter, all rights reserved
 * </pre>
 *
 * Makes CASL4J work with Spring Framework. Classes annotated with CASL4J
 * annotations become Spring beans. They can have other beans injected or be
 * injected into other beans as well.
 * <p>
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */

package org.zahnleiter.casl4j.spring;
