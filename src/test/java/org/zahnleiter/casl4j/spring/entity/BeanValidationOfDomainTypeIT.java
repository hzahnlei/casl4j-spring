package org.zahnleiter.casl4j.spring.entity;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.annotation.Validated;
import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.BeanValidated;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.domain.entity.type.DomainType;
import org.zahnleiter.casl4j.domain.service.DomainService;

/**
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@SpringBootTest(classes = { BeanValidationOfDomainTypeIT.TestConfig.class, BeanValidationConfiguration.class })
@Tag("IT")
@Tag("slow")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class BeanValidationOfDomainTypeIT {

    // ---- Tests --------------------------------------------------------------

    @Test
    void Constraints_on_domain_types_are_enforced_on_service_call() {
        final var invalidName = new TestDomainType("");
        try {
            this.testService.toUpper(invalidName);
        } catch (final ConstraintViolationException cause) {
            assertEquals(1, cause.getConstraintViolations().size());
            cause.getConstraintViolations() //
                    .forEach(violation -> assertEquals("Name must not be blank.", violation.getMessage()));
        }

    }

    @Test
    void Constraints_on_domain_objects_are_enforced_on_service_call() {
        final var invalidDomainObject = new TestDomainObject();
        try {
            this.testService.toUpper(invalidDomainObject);
        } catch (final ConstraintViolationException cause) {
            assertEquals(1, cause.getConstraintViolations().size());
            cause.getConstraintViolations() //
                    .forEach(violation -> assertEquals("List of names must not be empty.", violation.getMessage()));
        }

    }

    // ---- Implementation details ---------------------------------------------

    /**
     * The service, that is supposed to perform bean validation has to be a Spring
     * bean.
     *
     * @author Holger Zahnleiter
     */
    @SpringBootConfiguration
    public static class TestConfig {

        @Bean
        TestService service() {
            return new TestService();
        }

    }

    /**
     * Observe the {@link BeanValidated} annotation. It is the technology/framework
     * agnostic equivalent of Spring's {@link Validated}. Wit a little magic,
     * {@link BeanValidated} instructs spring to apply the {@link Valid} annotation.
     *
     * @author Holger Zahnleiter
     */
    @DomainService
    @BeanValidated
    public static class TestService {

        public @Valid TestDomainType toUpper(final @Valid TestDomainType name) {
            return new TestDomainType(name.value.toUpperCase());
        }

        public @Valid List<TestDomainType> toUpper(final @Valid TestDomainObject domainObject) {
            return domainObject.names.stream() //
                    .map(name -> name.value.toUpperCase()) //
                    .map(TestDomainType::new) //
                    .collect(toList());
        }

    }

    @DomainType
    public static class TestDomainType {

        /**
         * Bean validation constraint applied to a domain type.
         */
        @NotBlank(message = "Name must not be blank.")
        public final String value;

        public TestDomainType(final String value) {
            this.value = value;
        }

    }

    @Autowired
    TestService testService;

    @DomainEntity
    @Aggregate
    public static class TestDomainObject {

        /**
         * Bean validation constraint applied to a domain object.
         */
        @NotEmpty(message = "List of names must not be empty.")
        public final List<TestDomainType> names;

        public TestDomainObject(final TestDomainType... names) {
            this.names = asList(names);
        }

    }

}
