package org.zahnleiter.casl4j.spring.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.transaction.annotation.Propagation.NOT_SUPPORTED;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransaction;
import org.zahnleiter.casl4j.spring.testtools.TestPersonEntity;
import org.zahnleiter.casl4j.spring.testtools.TestPersonJpaRepository;
import org.zahnleiter.casl4j.spring.testtools.TestPersonService;
import org.zahnleiter.casl4j.spring.testtools.TestPersonUseCase;

/**
 * Transactions are controlled by the use case layer.
 * <p>
 * This is just a library. There is no Spring Boot application nor configuration
 * etc. Therefore, we need add relatively many annotations to get the Spring
 * Data components up and running.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DataJpaTest
@Tag("IT")
@Tag("slow")
@EnableJpaRepositories(basePackageClasses = TestPersonJpaRepository.class)
@EnableTransactionManagement
@ContextConfiguration(classes = { TestPersonJpaRepository.class, BusinessTransactionManagerImpl.class,
        TestPersonService.class, TestPersonUseCase.class, BusinessTransactionAspect.class })
@EntityScan(basePackageClasses = TestPersonEntity.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
// We do programmatic transaction handling herein
@Transactional(propagation = NOT_SUPPORTED)
@EnableAspectJAutoProxy
class AnnotatedTransactionIT {

    @Autowired
    private TestPersonJpaRepository personRepository;

    @Autowired
    private TestPersonUseCase useCase;

    @BusinessTransaction
    @Test
    void Data_is_persisted_if_transaction_succeeds() throws Exception {
        final var philipp = new TestPersonEntity("Philipp");
        assertNull(philipp.id);
        this.useCase.createWithoutError(philipp);
        assertNotNull(philipp.id);
        assertNotNull(this.personRepository.findByName("Philipp"));
    }

    @BusinessTransaction
    @Test
    void Nothing_is_persisted_if_transaction_fails() throws Exception {
        final var johanna = new TestPersonEntity("Johanna");
        try {
            assertNull(johanna.id);
            this.useCase.createWithError(johanna);
        } catch (final Throwable cause) {
            assertEquals("Failed intentionally", cause.getMessage());
        }
        assertNull(this.personRepository.findByName("Johanna"));
    }

}
