package org.zahnleiter.casl4j.spring.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.transaction.annotation.Propagation.NOT_SUPPORTED;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransactionManager;
import org.zahnleiter.casl4j.spring.testtools.TestPersonEntity;
import org.zahnleiter.casl4j.spring.testtools.TestPersonJpaRepository;
import org.zahnleiter.casl4j.spring.testtools.TestPersonService;

/**
 * Transactions are controlled by the use case layer.
 * <p>
 * This is just a library. There is no Spring Boot application nor configuration
 * etc. Therefore, we need add relatively many annotations to get the Spring
 * Data components up and running.
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DataJpaTest
@Tag("IT")
@Tag("slow")
@EnableJpaRepositories(basePackageClasses = TestPersonJpaRepository.class)
@EnableTransactionManagement
@ContextConfiguration(classes = { TestPersonJpaRepository.class, BusinessTransactionManagerImpl.class,
        TestPersonService.class })
@EntityScan(basePackageClasses = TestPersonEntity.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
// We do programmatic transaction handling herein
@Transactional(propagation = NOT_SUPPORTED)
class ProgrammaticTransactionIT {

    @Autowired
    private TestPersonJpaRepository personRepository;

    @Autowired
    private BusinessTransactionManager txManager;

    @Autowired
    private TestPersonService service;

    @Test
    void Entity_is_persisted_even_without_transaction() {
        final var holger = new TestPersonEntity("Holger");
        assertNull(holger.id);
        this.service.create(holger);
        assertNotNull(holger.id);
        assertNotNull(this.personRepository.findByName("Holger"));
    }

    @Test
    void Data_is_persisted_if_transaction_succeeds() throws Exception {
        try (final var tx = this.txManager.beginNewBusinessTransaction()) {
            final var nina = new TestPersonEntity("Nina");
            assertNull(nina.id);
            this.service.create(nina);
            tx.commit();
            assertNotNull(nina.id);
        }
        assertNotNull(this.personRepository.findByName("Nina"));
    }

    @Test
    void Nothing_is_persisted_if_transaction_fails() throws Exception {
        try (final var tx = this.txManager.beginNewBusinessTransaction()) {
            final var thomas = new TestPersonEntity("Thomas");
            assertNull(thomas.id);
            this.service.create(thomas);
            assertNotNull(thomas.id);
            throw new Exception("Failed intentionally");
        } catch (final Throwable cause) {
            // Auto-close automatically rolls back
            assertEquals("Failed intentionally", cause.getMessage());
        }
        assertNull(this.personRepository.findByName("Thomas"));
    }

}
