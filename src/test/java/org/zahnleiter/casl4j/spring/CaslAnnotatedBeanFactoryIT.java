package org.zahnleiter.casl4j.spring;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.zahnleiter.casl4j.common.OffsetDateTimeFactory;
import org.zahnleiter.casl4j.common.RandomUuidFactory;
import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.domain.usecase.UseCase;

/**
 * <p>
 * Clean Architecture Support Library for Java - Spring
 * <br>
 * (c) 2022 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@SpringBootTest(classes = { CaslAnnotatedBeanFactory.class, CaslSpringBootApplication.class })
@Tag("IT")
@Tag("slow")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class CaslAnnotatedBeanFactoryIT {

    @UseCase
    public static class SomeUseCase {
    }

    @Autowired
    public SomeUseCase someUseCase = null;

    @Test
    void UseCase_annotated_classes_become_Spring_beans() {
        assertNotNull(this.someUseCase);
    }

    @DomainService
    public static class SomeDomainService {
    }

    @Autowired
    public SomeDomainService someDomainService = null;

    @Test
    void DomainService_annotated_classes_become_Spring_beans() {
        assertNotNull(this.someDomainService);
    }

    @Autowired
    public OffsetDateTimeFactory offsetDateTimeFactory = null;

    @Test
    void Default_OffsetDateTimeFactoryImpl_bean_is_provided() {
        assertNotNull(this.offsetDateTimeFactory);
    }

    @Autowired
    public RandomUuidFactory uuidFactory = null;

    @Test
    void Default_UuidFactoryRandomImpl_bean_is_provided() {
        assertNotNull(this.uuidFactory);
    }

}
