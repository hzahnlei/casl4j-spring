package org.zahnleiter.casl4j.spring.testtools;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TestPersonJpaRepository
		extends JpaRepository<TestPersonEntity, Long>, JpaSpecificationExecutor<TestPersonEntity> {

	TestPersonEntity findByName(final String name);

}