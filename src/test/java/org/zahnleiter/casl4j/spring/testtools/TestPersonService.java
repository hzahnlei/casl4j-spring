package org.zahnleiter.casl4j.spring.testtools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestPersonService {

	@Autowired
	private TestPersonJpaRepository repo;

	public void create(final TestPersonEntity person) {
		this.repo.save(person);
	}

}
