package org.zahnleiter.casl4j.spring.testtools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransaction;

@Component
public class TestPersonUseCase {

	@Autowired
	private TestPersonService service;

	@BusinessTransaction
	public void createWithoutError(final TestPersonEntity person) {
		this.service.create(person);
	}

	@BusinessTransaction
	public void createWithError(final TestPersonEntity person) throws Exception {
		this.service.create(person);
		throw new Exception("Failed intentionally");
	}
}
