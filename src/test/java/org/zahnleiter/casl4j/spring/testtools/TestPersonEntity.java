package org.zahnleiter.casl4j.spring.testtools;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.AccessType;
import org.springframework.data.annotation.AccessType.Type;

@Entity
@Table
@AccessType(Type.FIELD)
public class TestPersonEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@Column(nullable = false, unique = true)
	public String name;

	public TestPersonEntity() {
	}

	public TestPersonEntity(final String name) {
		this.name = name;
	}

}