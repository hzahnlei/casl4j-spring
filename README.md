# CASL4J - Clean Architecture Support Library for Java - Spring

Library that helps structure your Java/Spring Boot code according to "Clean Architecture".

Holger Zahnleiter, 2021-02-08

## Introduction

This library makes the CASL4J base library work with the Spring Framework.
Classes annotated with CASL4J annotations become Spring beans.
They can have other beans injected or be injected into other beans as well.
As they have become Spring beans they are instantiated by Spring automatically.

Remember, CASL4J itself is technology agnostic.
Furthermore, Clean Architecture mandates domain logic to be technology agnostic too.
As a consequence, classes from the domain logic wold have be to instantiated manually instead of being instantiated by Spring automatically.
This would be a lot of tedious boilerplate.
However, with this library you can Springify your agnostif domain logic.
For example: A class annotated `@UseCase` now becomes a Spring bean.
You do not have to instatiate it manually.
And, as we are following Spring's recommendation to provide a proper constructor, Spring will automatically inject required beans by means of constructor injection.

The `casl4j-archcheck` project brings you a Gradle plug-in which you can use to check compliance of domain logic to the rules of Clean Architecture.
This project however, in addition to the functionality described above, introduces an additional Gradle plug-in to check whether your Spring application also complies to the rules of Clean Architecture.

## How to Add this Library to Your Project

### Cloning and Downloading

Of cause you can clone this project, compile it and push the artifacts into your local Maven repository and use it from there.

Furtheremore, the artifacts can be downloaded from GitLab.
Do so and add the JAR to your project's class-path.

### Maven Repository

The preferred way is to add the artifacts (JAR, source and Javadoc) to your project as Maven or Gradle dependencies.
This requires that you first add my GitLab Maven repository to your Gradle or Maven build file like shown next.

Then, just add a dependency to the library.

```groovy
repositories {
    ...
    maven {
       url 'https://gitlab.com/api/v4/projects/24260622/packages/maven'
       name '_CASL4J-spring_GitLab_'
    }
    ...
}
...
dependencies {
    ...
	implementation 'org.zahnleiter:casl4j-spring:1.0.0'
    ...
}
```

## How to Add this Gradle Plug-in to Your Project

### Cloning and Downloading

See above.

### Maven Repository

The preferred way is to add the artifacts (JAR, source and Javadoc) to your project as Maven or Gradle dependencies.
This requires that you first add my GitLab Maven repository to your Gradle or Maven build file like shown next.

We cannot just activate this plug-in by adding its ID to the `plugins` section of `build.gradle` as this is not a default Gradle plug-in.
In `settings.gradle` you have to introduce my custom repositories from which to pull the plug-in:

```groovy
pluginManagement {
	repositories {
        ...
        // Requires all my GitLab repos that belog to my CASL4J project.
        maven {
            url 'https://gitlab.com/api/v4/projects/36778618/packages/maven'
            name '_CASL4J_GitLab_'
        }
        maven {
            url 'https://gitlab.com/api/v4/projects/36830434/packages/maven'
            name '_CASL4J-archcheck_GitLab_'
        }
        maven {
            url 'https://gitlab.com/api/v4/projects/24260622/packages/maven'
            name '_CASL4J-spring_GitLab_'
        }
        ...
    }
}
```

In `build.gradle` now add the plug-in:

```groovy
plugins {
    ...
    id 'org.zahnleiter.casl4j.spring-application-architecture-check' version '1.0.0'
    ...
}
```

The plug-in ID is not sufficient, as this is no core plug-in but a custom plug-in.
The version needs to be provided as well.

```bash
./gradlew springApplicationArchitectureCheck
```

## Disclaimer

This is a private, free, open source and non-profit project (see LICENSE file).
Use on your own risk.
I am not liable for any damage caused.

I am a private person, not associated with any companies mentioned herein.

## Credits

The ideas presented herein are not my own.
I am building on the ideas and experience of countless software developers and software architects before me.
Also, I am using many open source and free software and services.
Thank you:

-  [GitLab](https://gitlab.com)
-  [Visual Studio Code](https://code.visualstudio.com) - Microsoft
-  [Eclipse](https://www.eclipse.org/downloads/) - Eclipse Foundation
-  [Git](https://git-scm.com)
-  [Gradle](https://gradle.org)
-  [Maven](https://maven.apache.org) - Apache Software Foundation
-  [ArchUnit](https://www.archunit.org) - TNG Technology Consulting GmbH
-  [Spring Framework](https://spring.io/projects/spring-framework) - VMware
-  [H2 Database](https://www.h2database.com/html/main.html)
-  Many more that I might have forgotten or even not be aware of using...
