# Change Log

## 1.0.0 - Initial Version (2021-02-08)

### Functional

-  Converts classes with CASL4J annotations to Spring beans.
-  Implements transaction manager abstraction.
-  Rudimentary architecture check Gradle plug-in for Spring applications.

### Non-Functional

-  Gradle build script
-  Publish module and plug-in to GitLab Maven repository
-  Produce coverage report and push to GitLab for display and merge requests
-  Push test report to GitLab for display
